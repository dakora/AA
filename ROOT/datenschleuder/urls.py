from django.conf.urls import url

from . import views

app_name = 'datenschleuder'

urlpatterns = [
    url(r'^$', views.datenschleuder_main, name='index'),
]
