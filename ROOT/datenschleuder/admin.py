from django.contrib import admin

from .models import Subscriber


@admin.register(Subscriber)
class MemberAdmin(admin.ModelAdmin):
    pass
