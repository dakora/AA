from django.test import TestCase
from django_dynamic_fixture import G
from members.models import Member, Erfa, get_alien
from api import views
from datetime import date, timedelta
import itertools as it


def dict_product(dict_of_lists):
    var_names = sorted(dict_of_lists)
    return [dict(zip(var_names, prod)) for prod in it.product(*(dict_of_lists[var_name] for var_name in var_names))]


class StatisticsTestCase(TestCase):
    def setUp(self):
        erfa_variants = {
            'has_doppelmitgliedschaft': [True, False],
        }

        erfa_product = dict_product(erfa_variants)

        for combination in erfa_product:
            G(Erfa, **combination)

        member_variants = {
            'account_balance': [-200, 0, 200],
            'erfa': list(Erfa.objects.all()),
            'fee_override': [None, -10, 0, 2500],
            'fee_paid_until': [date.today() - timedelta(days=365), date.today(), date.today() + timedelta(days=365)],
            'is_active': [True, False],
            'membership_end': [date.today() - timedelta(days=1), date.today(), date.today() + timedelta(days=1)],
            'membership_reduced': [True, False],
            'membership_type': [Member.MEMBERSHIP_TYPE_HONORARY, Member.MEMBERSHIP_TYPE_SUPPORTER,
                                Member.MEMBERSHIP_TYPE_MEMBER],
        }

        member_product = dict_product(member_variants)

        for combination in member_product:
            G(Member, **combination, membership_start=date.today() - timedelta(days=730))

    def test_stats(self):
        erfa_stats = views._erfa_statistics()
        payment_stats = views._payment_stats()

        # The sum of members, supporters, and honorary members should equal the sum of normal, reduced, and special
        # paying members
        for row in erfa_stats[1:]:
            self.assertEqual(row[1] + row[2] + row[3], row[4] + row[5] + row[6])

        # The total number of members in all erfas should equal the total number of members in the payment stats
        self.assertEqual(sum(row[1] + row[2] + row[3] for row in erfa_stats[1:]), payment_stats[-1][1])
