# -*- coding: utf-8 -*-

import datetime
import json

import gpg
import pendulum
import re
from tabulate import tabulate
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.core import exceptions
from django.core.mail import EmailMessage
from django.db.models import Q, Sum
from django.db.models.functions import Coalesce
from django.http import JsonResponse
from django.shortcuts import HttpResponse, render
from django.template.loader import get_template, select_template

from ROOT.helper import i_request, str2bool
from ROOT.settings import (
    EMAIL_HOST_USER, EMAIL_MANAGING_DIRECTOR, FEE_NOTIFICATION_TIME,
    GPG_HOST_USER, GPG_MANAGING_DIRECTOR, GPG_HOME
)
from members.countryfield import COUNTRIES
from members.models import EmailToMember, Erfa, Member, send_or_refresh_data_record
from other.scheduler import *

RETURN_TYPE_JSON = 'json'
RETURN_TYPE_HTML = 'html'

MAIL_CREATE_NEW_USER = 'new_user'
MAIL_CREATE_DELAYED_PAYMENT = 'delayed_payment'


def _get_return_type(request):
    return_as = RETURN_TYPE_JSON
    if 'return_as' in i_request(request):
        return_as = i_request(request)['return_as']
    return return_as


def index(request):
    return render(request, 'api/index_api.html', {})


def country_analysis(request):
    return_as = _get_return_type(request)
    country_dict = {}

    for member in Member.objects.members_only():
        if member.address_country not in country_dict:

            country_name = ''
            for countryPair in COUNTRIES:
                if countryPair[0] == member.address_country:
                    country_name = countryPair[1]
            country_dict[member.address_country] = {
                'country': member.address_country,
                'country_name': country_name,
                'members': 0
            }
        country_dict[member.address_country]['members'] += 1

    if return_as == RETURN_TYPE_JSON:
        json_list = []
        for countryKey in sorted(country_dict):
            json_list.append([
                countryKey, country_dict[countryKey]['country_name'],
                country_dict[countryKey]['members']
            ])

        return HttpResponse(json.dumps(json_list), content_type='application/json')

    return HttpResponse('BAD FORMAT', content_type='text/html')


def zip_analysis(request):
    return_as = _get_return_type(request)

    country = 'DE'
    if 'country' in i_request(request):
        country = i_request(request)['country']

    member_list = Member.objects.filter(address_country=country)

    zip_member_count_dict = {}
    zip_member_count_total = 0
    zip_count_total = 0

    for member in member_list:
        if member.is_member():
            plz = member.get_plz()
            if plz != '':
                plz = plz[:2]
                if plz not in zip_member_count_dict:
                    zip_member_count_dict[plz] = 0
                    zip_count_total += 1
                zip_member_count_dict[plz] += 1
                zip_member_count_total += 1

    zip_member_count_list = []
    for item in zip_member_count_dict.items():
        zip_member_count_list.append(item)

    if return_as == RETURN_TYPE_JSON:
        return HttpResponse(json.dumps({'zip_member_count_list': zip_member_count_list,
                                        'zip_member_count_total': zip_member_count_total,
                                        'zip_count_total': zip_count_total}), content_type='application/json')

    elif return_as == RETURN_TYPE_HTML:
        parsed_template = get_template('api_response_table_simple.html').render({'dataList': zip_member_count_list})

        return HttpResponse('<p>Total Members: ' + str(zip_member_count_total) + '<br />Total ZIP Codes:'
                            + str(zip_count_total) + '</p>' + parsed_template, content_type='text/html')

    return HttpResponse('BAD FORMAT', content_type='text/html')


def member_info(request):
    """
    get infos about a member by chaos_number

    chaos_number
    separator

    :param request:
    :return:
    """

    separator = '<br>'

    if 'separator' in i_request(request):  # request.REQUEST:
        separator = i_request(request)['separator']  # request.REQUEST['separator']
    if separator == '\\n':
        separator = '\n'

    all_infos = ''
    if 'chaos_number' in i_request(request):
        member = Member.objects.get(chaos_number=int(i_request(request)['chaos_number']))
        all_infos += str(member) + separator
        all_infos += 'Name: ' + member.get_name() + separator
        all_infos += 'Address: ' + member.get_address() + separator
        all_infos += 'Country: ' + member.address_country + separator
        all_infos += 'PLZ: ' + member.get_plz() + separator
        if member.email_is_unknown():
            all_infos += 'email is unknown' + separator
        else:
            all_infos += separator.join(str(v) for v in member.get_emails())

    if all_infos == '':
        all_infos = 'nothing to do ' + ', '.join(i_request(request))

    return HttpResponse(all_infos, content_type='text')


def member_queue_data_record_email(request):
    """
    Send a data record email to the specified Member(s) no matter if anything in their data set changed. Will refresh
     already queued emails instead of queueing multiple emails.
    :param request: Should be a GET request with one or more parameters 'chaos_number', being the chaos_number of a
    Member
    :return: A JSON string with a dictionary of each chaos number paired with either 'queued' or 'not found'
    """
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number)
            send_or_refresh_data_record(member)
            response[chaos_number] = 'queued'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found'
    return HttpResponse(json.dumps(response), content_type='application/json')


def member_inactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=True)
            member.is_active = False
            member.save()
            response[chaos_number] = 'inactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already inactive'
    return HttpResponse(json.dumps(response), content_type='application/json')


def member_reactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=False)
            member.is_active = True
            member.save()
            response[chaos_number] = 'reactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already active'
    return HttpResponse(json.dumps(response), content_type='application/json')


def billing_cycle(request):
    """
    :param request:
    :return: HttpResponse (json)
    """
    del request

    booked = map(lambda member: {'member': str(member),
                                 'memberName': member.get_name(),
                                 'balance': member.account_balance} if member.execute_payment_if_due() else None,
                 Member.objects.all())
    booked = list(filter(lambda x: x is not None, booked))

    return HttpResponse(json.dumps(booked), content_type='application/json')


@daily()
def billing_cycle_cron():
    for member in Member.objects.all():
        member.execute_payment_if_due()


def _mail_create_by_id(mail_type, u_id):
    return _mail_create(mail_type, u_id)


def mail_create(request):
    mail_type = ''
    user_id = -1
    if MAIL_CREATE_NEW_USER in i_request(request):
        mail_type = MAIL_CREATE_NEW_USER
        user_id = i_request(request)[MAIL_CREATE_NEW_USER]
    elif MAIL_CREATE_DELAYED_PAYMENT in i_request(request):
        mail_type = MAIL_CREATE_DELAYED_PAYMENT

    response = _mail_create(mail_type, user_id)

    return HttpResponse(json.dumps(response), content_type='application/json')


def _get_delayed_payment_members():
    """Finds all members who are overdue with their payments. Members are only included if the payment was due more than
    FEE_NOTIFICATION_TIME (set in settings.py) months ago and the member is active and not a Doppelmitglied in any Erfa.
    This method will return an empty queryset, if FEE_NOTIFICATION_TIME is not a valid integer.

    :return: A queryset holding all overdue members.
    """
    if not isinstance(FEE_NOTIFICATION_TIME, int):
        print("Config value FEE_NOTIFICATION_TIME has to be an integer representing months.")
        return Member.objects.none()

    # To calculate if a member is overdue a date to compare against the paid_until_date is calculated like this:
    # today + 1 year - FEE_NOTIFICATION_TIME months
    # If the paid_until_date is less than this cut off date, then the payment is overdue.

    cut_off_date = pendulum.datetime.today().add(years=1).subtract(months=FEE_NOTIFICATION_TIME).date()

    return Member.objects.filter(
        fee_paid_until__lte=cut_off_date,
        account_balance__lt=0,
        is_active=True,
        erfa__has_doppelmitgliedschaft=False
    )


def _mail_create(mail_type, user_id):
    response = []
    member_list = []
    context_dict = {}

    mails_skipped = 0
    mails_errors = 0
    mails_added = 0

    if MAIL_CREATE_DELAYED_PAYMENT == mail_type:

        member_list = _get_delayed_payment_members()
        subject = 'Zahlungsverzug'  # TODO: Translate!
        template_prefix = 'mail_templates/mail_delayedpayment_'
        mail_to_send_type = EmailToMember.SEND_TYPE_DELAYED_PAYMENT
        # +2 month AND not empty
    else:
        return {'state': 'error'}

    members_i_cannot_mail = []
    for member in member_list:
        if not member.is_active:  # yeah, looks bad, waiting for an better idea to do that -.-
            continue
        context_dict['member'] = member

        # TODO: complete the calculation!
        context_dict['toPay'] = '{0:.2f}'.format(member.get_money_to_pay() / 100)

        template = select_template([template_prefix + member.address_country + '.txt.html',
                                    template_prefix + 'EN.txt.html'])
        parsed_template = template.render(context_dict)

        try:
            mail = member.get_primary_mail()
            if not mail:
                # member has no mail address, i don't care!
                mails_skipped += 1
                members_i_cannot_mail.append('has no Email Address: ' + str(member))
                continue
            msg = EmailToMember()

            msg.member = member
            msg.body = parsed_template
            msg.subject = subject
            msg.created = datetime.now()
            msg.email_type = mail_to_send_type

            try:
                # no problem with multiple existing mailadresses, because foreign key=member/chaosnr
                msg_exists = EmailToMember.objects.filter(member=member, subject=subject)
                if msg_exists.count() > 0:
                    mails_skipped += 1
                    members_i_cannot_mail.append('SKIPPED: Mail with subject "' + subject +
                                                 '" already exists in queue for: ' + str(member))
                else:
                    mails_added += 1
                    msg.save()
                    response.append(str(mail.email_address))

            except Exception as ex:
                mails_errors += 1
                print(ex)
                members_i_cannot_mail.append('ERROR: checkExistingMails ' + str(member) + ' Ex: ' + str(ex))
        except exceptions.ObjectDoesNotExist:
            # member has no mail address, i don't care!
            members_i_cannot_mail.append('has no Email Address: ' + str(member))
            mails_skipped += 1
        except Exception as ex:
            mails_errors += 1
            print(ex)
            members_i_cannot_mail.append('ERROR: ' + str(member) + ' Ex: ' + str(ex))

    return ['mails_added: ' + str(mails_added), 'mails_skipped: ' + str(mails_skipped), 'mails_errors: ' +
            str(mails_errors)] + members_i_cannot_mail + response


def mail_send_next(request):
    mail_to_send = EmailToMember.objects.first()
    if mail_to_send:
        response = mail_to_send.send()
    else:
        response = {'state': 'nothing to send'}
    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_send_all(request):
    response = [mail.send() for mail in EmailToMember.objects.all()]

    if len(response) == 0:
        response = {'state': 'nothing to send'}

    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_still_to_send(request):
    number_of_mails = EmailToMember.objects.all().count()
    return HttpResponse(json.dumps({'numberOfMailsToSend': number_of_mails}), content_type='application/json')


def search_member_db(request):
    """Handles member DB search requests returning HTML-table-rows"""
    context = {}
    if request.method == 'POST':
        params = request.POST
        check_empty_first_name = params.get('check_empty_first_name')
        check_empty_last_name = params.get('check_empty_last_name')
        check_empty_address = params.get('check_empty_address')
        check_empty_country = params.get('check_empty_country')
        check_empty_email = params.get('check_empty_email_address')
        if 'true' in (check_empty_address, check_empty_country, check_empty_first_name, check_empty_last_name,
                      check_empty_email):
            # Search for empty fields
            q = Member.objects.all()
            if check_empty_first_name == 'true':
                q = q.filter(Q(first_name__exact='') | Q(first_name__isnull=True))
                print(q)
            if check_empty_last_name == 'true':
                q = q.filter(Q(last_name__exact='') | Q(last_name__isnull=True))
            if check_empty_address == 'true':
                # Filter for datasets with all three address field empty
                q = q.filter(
                    (
                        (Q(address_1__exact='') |
                         Q(address_1__isnull=True)) &
                        (Q(address_2__exact='') |
                         Q(address_2__isnull=True)) &
                        (Q(address_3__exact='') |
                         Q(address_3__isnull=True))
                    ) | Q(address_unknown=True)
                )
            if check_empty_country == 'true':
                q = q.filter(Q(address_country__exact='') | Q(address_country__isnull=True))
            if check_empty_email == 'true':
                q = q.exclude(emailaddress__isnull=False)

            context['results'] = q
        else:
            first_name = params.get('first_name', '')
            last_name = params.get('last_name', '')
            chaos_number = params.get('chaos_id', '')
            address = params.get('address', '')
            email = params.get('email_address', '')
            fee_reduced = str2bool(params.get('fee_is_reduced', 'false'))
            is_active = str2bool(params.get('is_active', 'false'))
            apply_filters = str2bool(params.get('apply_filters', 'false'))
            q = Member.objects.all()
            if first_name != '':
                q = q.filter(first_name__icontains=first_name)
            if last_name != '':
                q = q.filter(last_name__icontains=last_name)
            if chaos_number != '':  # maybe check for illegal characters in the future
                q = q.filter(chaos_number=chaos_number)
            if address != '':
                q = q.filter(Q(address_1__icontains=address) | Q(address_2__icontains=address)
                             | Q(address_3__icontains=address))
            if email != '':
                q = q.filter(Q(emailaddress__email_address__icontains=email))
            if apply_filters:
                q = q.filter(membership_reduced=fee_reduced, is_active=is_active)

            context['results'] = q
        return render(request, 'api/member_search_result.html', context)
    return HttpResponse('Neither Get nor Post')  # HttpResponse('BAD FORMAT', content_type='text/html')


def _erfa_statistics():
    erfa_statistics = [
        ['Erfa', 'Mitglied', 'Förderm.', 'Ehrenm.', 'Vollzahler', 'Ermäßigt', 'Spezial', 'Vollzahler €', 'Ermäßigt €',
         'Spezial €', 'Summe €']]

    for erfa in Erfa.objects.all().order_by('long_name'):
        erfa_stat = []

        m = Member.objects.filter(erfa=erfa)

        # Name of the Erfa
        erfa_stat.append(str(erfa))

        # Count of different membership types
        for membership_type in [Member.MEMBERSHIP_TYPE_MEMBER, Member.MEMBERSHIP_TYPE_SUPPORTER,
                                Member.MEMBERSHIP_TYPE_HONORARY]:
            erfa_stat.append(m.filter(membership_type=membership_type).count())

        # Count of members by fee types
        sum_full = m.filter(fee_override__isnull=True, membership_reduced=False)
        sum_red = m.filter(fee_override__isnull=True, membership_reduced=True)
        sum_spec = m.filter(fee_override__isnull=False)

        erfa_stat.extend([x.count() for x in [sum_full, sum_red, sum_spec]])

        # Expected fees by membership type
        erfa_stat.append(sum_full.count() * settings.FEE / 100.0)
        erfa_stat.append(sum_red.count() * settings.FEE_REDUCED / 100.0)
        erfa_stat.append(sum_spec.aggregate(spec_fee=Coalesce(Sum('fee_override'), 0))['spec_fee'] / 100.0)

        # Total expected fees per Erfa
        erfa_stat.append(sum(erfa_stat[-3:]))

        erfa_statistics.append(erfa_stat)

    return erfa_statistics


def _payment_stats():
    payment_stats = [['', 'Anzahl', '>2 Monate im Rückstand', 'Ausstände >2 Monate (€)']]
    # Since the fee_paid_until field is set to when the next payment would be due, it is one year ahead of your last
    # due date. Thus we take two months off of this date (12 - 2) to get all members 2 months over their last due date.
    cut_off_date = date.today() + relativedelta(months=10)

    m = Member.objects.all()

    for membership_type, type_desc in [(Member.MEMBERSHIP_TYPE_MEMBER, 'Mitglieder'),
                                       (Member.MEMBERSHIP_TYPE_SUPPORTER, 'Fördermitglieder')]:
        o = m.filter(membership_type=membership_type)
        for q, desc in [(o.filter(fee_override__isnull=True, membership_reduced=False), 'Vollzahler'),
                        (o.filter(fee_override__isnull=True, membership_reduced=True), 'Ermäßigt'),
                        (o.filter(fee_override__isnull=False), 'Spezial')]:

            # Name and count
            line = ['{} ({})'.format(type_desc, desc), q.count()]

            # Count overdue >2 months
            r = q.filter(account_balance__lt=0, fee_paid_until__lt=cut_off_date, erfa__has_doppelmitgliedschaft=False)
            line.append(r.count())

            # fees overdue >2 months
            line.append(abs(r.aggregate(s=Coalesce(Sum('account_balance'), 0))['s'] / 100.0))
            payment_stats.append(line)

    payment_stats.append(['Ehrenmitglieder', m.filter(membership_type=Member.MEMBERSHIP_TYPE_HONORARY).count(), 0, 0])

    data_grid = zip(*payment_stats[1:])
    next(data_grid)
    payment_stats.append(['Summen'] + [sum(col) for col in data_grid])

    return payment_stats


@monthly()
def _format_erfa_statistics():
    context_dict = {'erfa_stats': tabulate(_erfa_statistics(), headers='firstrow'),
                'payment_stats': tabulate(_payment_stats(), headers='firstrow', floatfmt=['', '', '', '.2f'])}

    template = select_template(['mail_templates/mail_erfa_statistic.html', ])
    parsed_template = template.render(context_dict)

    with gpg.Context(armor=True) as c:
        c.set_engine_info(gpg.constants.protocol.OpenPGP, home_dir=GPG_HOME)
        sender_key = c.get_key(GPG_HOST_USER, secret=True)
        c.signers = [sender_key]

        recipient_key = c.get_key(GPG_MANAGING_DIRECTOR)
        encrypted_body, _, _ = c.encrypt(parsed_template.encode(), recipients=[sender_key, recipient_key],
                                         always_trust=True, sign=True)

        send_email = EmailMessage(
            subject='Monatliche Statistik',
            body=encrypted_body.decode('utf-8'),
            from_email=EMAIL_HOST_USER,
            to=[EMAIL_MANAGING_DIRECTOR, ],
            bcc=[EMAIL_HOST_USER, ]
        )
        send_email.send(False)

    return parsed_template


def get_erfa_statistics(_):
    return HttpResponse('<pre>' + _format_erfa_statistics + '</pre>')


@daily()
def exit_members():
    """
    A daily check to remove all resigned members from the database. Resignation confirmation emails should have been
    sent already, because this routine touches every row in the database with a membership_end date in the past on every
    run. This may be inefficient, but simple.
    :return:
    """
    for member in Member.objects.filter(Q(membership_end__isnull=False) & Q(membership_end__lte=date.today())):
        member.exit()


def create_delayed_payment_export():
    member_list = _get_delayed_payment_members()
    # final_file_string = ''
    import csv

    with open('/tmp/delayed_payment_export.csv', 'w', newline='\n') as csvfile:
        export_writer = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        export_writer.writerow(['chaos_number',
                                'first_name',
                                'last_name',
                                'address_1',
                                'address_2',
                                'address_3',
                                'address_country',
                                'state',
                                'entry_date',
                                'yearly_fee',
                                'last_payment',
                                'paid_until',
                                'balance',
                                'open_years',
                                'fee_to_pay'])
        for member in member_list:
            row_arr = [member.chaos_number, member.first_name, member.last_name, member.address_1, member.address_2,
                       member.address_3, member.address_country, 'aktiv', member.membership_start,
                       member.get_annual_fee(), member.fee_last_paid, member.fee_paid_until,
                       member.get_balance_readable(), 7, member.get_money_to_pay()]
            export_writer.writerow(row_arr)


def drop_transactions(request):
    # http://localhost:8000/api/drop_transactions?reallydroptransactions=yes
    _request = i_request(request)
    response = {'errorMessage': '', 'wasCleared': False, 'transactionsDropped': 0}
    if 'reallydroptransactions' in _request and _request['reallydroptransactions'] == 'yes':
        try:
            from import_app.models import Transaction
            all_transactions = Transaction.objects.all()
            count_transactions = all_transactions.count()
            all_transactions.delete()
            response['transactionsDropped'] = count_transactions
            response['wasCleared'] = True
        except Exception as ex:
            response['errorMessage'] = str(ex)

    return JsonResponse(response)


def bulk_address_unknown(request):
    changes = {'success': 0, 'error': 0, 'no_data': False}
    if request.method == 'POST':
        chaos_numbers = request.POST['chaos_numbers'].split('\n')
        for chaos_number in chaos_numbers:
            nr = re.search('([0-9]+)', chaos_number).group(1)
            print(nr)
            m = Member.objects.get(chaos_number__exact=nr)
            print(m)
            try:
                m.address_unknown = True
                m.save()
                changes['success'] += 1
            except:
                changes['error'] += 1
    else:
        changes['no_data'] = True
    return JsonResponse(changes)
