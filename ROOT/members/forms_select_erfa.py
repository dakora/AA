from django import forms
from members.models import Erfa


class SelectDoppelErfaForm(forms.Form):
    erfa = forms.ModelChoiceField(queryset=Erfa.objects.filter(has_doppelmitgliedschaft=True), empty_label=None)
