from django import forms


class DeleteEmailsForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )
    mailsfield = forms.CharField(widget=forms.Textarea)
