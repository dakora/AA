from django.conf.urls import url

from . import views

app_name = 'members'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^import_members/', views.run_import_members, name='import_members'),
    url(r'^add_member/', views.add_member, name='add_member'),
    url(r'^delete_emails/', views.delete_emails, name='delete_emails'),
    url(r'^search_form/', views.search_member_db, name='search_form'),
    url(r'^member_address_unknown/', views.member_address_unknown, name='memberAddressUnknown'),

    url(r'^statistics/monthly/', views.monthly_statistics, name='monthly_statistics'),
    url(r'^statistics/overview/', views.general_statistics, name='general_statistics'),

    url(r'^erfaabgleich/export/(?P<all_erfas>all)?', views.erfaabgleich_export, name='erfaabgleich_export'),
    url(r'^erfaabgleich/import/(?P<all_erfas>all)?', views.erfaabgleich_import, name='erfaabgleich_import'),
    url(r'^erfaabgleich/cashpoint_export/', views.cashpoint_export, name='cashpoint_export'),
    url(r'^erfaabgleich/vereinstisch_import/', views.vereinstisch_import, name='vereinstisch_import'),
    url(r'^erfaabgleich/vereinstisch_export/', views.vereinstisch_export, name='vereinstisch_export'),
    url(r'^erfaabgleich/', views.erfaabgleich, name='erfaabgleich'),
    url(r'^show_transaction_log/$', views.show_transaction_log, name='show_transaction_log'),
    url(r'^show_transaction_log/(\d+)$', views.show_transaction_log, name='show_member_transaction_log'),
    url(r'^anti_transaction/(\d+)$', views.anti_transaction, name='anti_transaction'),
    url(r'^erfa/(?P<erfa>\d+)$', views.ErfaListView.as_view(template_name='member_list.html'), name='erfa'),
    url(r'^select_erfa/$', views.erfa_select_form, name='select_erfa'),
]
