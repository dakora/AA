$('.tablesorter').tablesorter();

$('#submit').click(function()
{
  if($('#id_clear_results_before_query').is(':checked')){
    $('#results > tbody > tr').remove();
  }
    $.ajax({
        url: document.location.origin+'/api/search_member_db/',
        type:'POST',
        data:
        {
          csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
          first_name: $('#id_member_first_name').val(),
          last_name: $('#id_member_last_name').val(),
          chaos_id: $('#id_member_chaos_id').val(),
          address: $('#id_member_address').val(),
          email_address: $('#id_member_email_address').val(),

          fee_is_reduced: $('#id_member_fee_reduced').is(':checked'),
          is_active: $('#id_is_active').is(':checked'),
          apply_filters: $('#id_apply_filters').is(':checked'),
          check_empty_first_name: $('#id_check_empty_0').is(':checked'),
          check_empty_last_name: $('#id_check_empty_1').is(':checked'),
          check_empty_address: $('#id_check_empty_2').is(':checked'),
          check_empty_country: $('#id_check_empty_3').is(':checked'),
          check_empty_email_address: $('#id_check_empty_4').is(':checked'),
          check_empty_activate: $('#id_check_empty_activate').is(':checked'),
        },
        success: function(msg)
        {
          $('#results tbody').append(msg);
          $('.tablesorter').trigger('update');
        },
        error: function(xhr, msg, error)
        {
          console.log(xhr);
        }
    });
    return false;
});
