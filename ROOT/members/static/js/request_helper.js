function clearElement(elementString) {
    var con = $(elementString);
    con.empty();
}


function simpleRequestHtml(url, msg) {
    var con = $('#mainContainer');
    //con.empty();
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            con.append(msg);
            con.append(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR: '" + textStatus + "' error thrown: '" + errorThrown + "'");
        }
    });
}


function simpleRequest(url, msg) {
    var con = $('#mainContainer');
    //con.empty();
    jQuery.ajax({
        type: 'GET',
        url: url,
        //crossDomain: true,
        dataType: 'json', // ** ensure you add this line **
        success: function(data) {
            con.append(msg);

            jQuery.each(data, function(index, item) {
                if($.isPlainObject(item)) {
                    jQuery.each(item, function(indexSubArray, itemSubArray) {
                        con.append(itemSubArray +" ");
                    });
                } else {
                    con.append(item);
                }
                con.append("<br/>");
                //now you can access properties using dot notation
            });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR: '" + textStatus + "' error thrown: '" + errorThrown + "'");
        }
    });
}


function simpleRequestTable(url, msg, headerAr) {
    var con = $('#mainContainer');
    //con.empty();
    jQuery.ajax({
        type: 'GET',
        url: url,
        //crossDomain: true,
        dataType: 'json', // ** ensure you add this line **
        success: function(data) {
            con.append(msg);
            tableStr = '';
            tableStr += '<div class="table-responsive"><table class="table table-striped tablesorter tableAuto">';

            if (headerAr != undefined) {
                tableStr += '<thead><tr>';
                jQuery.each(headerAr, function(index, item) {
                    tableStr += '<th>' + item + '</th>';
                });
                tableStr += '<tr></thead>';
            }

            tableStr += "<tbody>";
            jQuery.each(data, function(index, item) {
                tableStr += "<tr>";
                if($.isPlainObject(item) || $.isArray(item)) {
                    jQuery.each(item, function(indexSubArray, itemSubArray) {
                        tableStr += '<td>' + itemSubArray + '</td>';
                    });
                } else {
                    tableStr += '<td>' + item + '</td>';
                }
                tableStr += "</tr>";
                //now you can access properties using dot notation
            });
            tableStr += "</tbody></table></div>";
            con.append(tableStr);
            $(".tablesorter").tablesorter();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR: '" + textStatus + "' error thrown: '" + errorThrown + "'");
        }
    });
}
