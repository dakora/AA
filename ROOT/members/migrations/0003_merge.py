# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_auto_20161002_1357'),
        ('members', '0002_balancetransactionlog_created_on'),
    ]

    operations = [
    ]
