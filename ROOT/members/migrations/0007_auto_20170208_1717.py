# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0006_auto_20170205_1645'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='EmailArchive',
            new_name='ArchivedEmail',
        ),
        migrations.RenameField(
            model_name='archivedemail',
            old_name='email',
            new_name='email_address',
        ),
        migrations.AlterField(
            model_name='emailtomember',
            name='email_type',
            field=models.CharField(choices=[('def', 'default'), ('dml', 'data record'), ('wlc', 'welcome'), ('dep', 'delayed payment'), ('gpg', 'gpg error')], default='def', max_length=3),
        ),
    ]
