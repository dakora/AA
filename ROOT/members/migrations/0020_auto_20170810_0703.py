# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-10 07:03
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0019_auto_20170809_1520'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='fee_last_paid',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_paid_until',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_start',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
