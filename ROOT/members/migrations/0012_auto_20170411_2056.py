# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0011_auto_20170405_0138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='fee_last_paid',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_paid_until',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_start',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
