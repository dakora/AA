# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0010_auto_20170404_2025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='fee_last_paid',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_paid_until',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='member',
            name='last_update',
            field=models.DateField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_start',
            field=models.DateField(),
        ),
    ]
