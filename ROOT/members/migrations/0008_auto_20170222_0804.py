# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0007_auto_20170208_1717'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='emailaddress',
            unique_together=set([('member', 'email_address')]),
        ),
    ]
