# Administration Association

This app is written by members of the Chaos Computer Club to help its administration, the office, to manage the
club's memberships. It is not meant to be awesome, just better than DBaseII, which it does pretty well.

The main focuses in development are:
* Replace the DBaseII solution used since at least 1995
* Enable working in parallel
* Be easily modifiable and extensible
* Sending *encrypted* mails to members about any changes in their status
* Reading and processing bank statements in CSV format

You are welcome to fork, send pull request and much more, but the goal of this piece of software will remain running the
Chaos Computer Club's memberships.

## Project Webpage

Find the source and downloads at https://gitlab.com/pythonfoo/AA

## Setup

First create a file ```ROOT/ROOT/settings_production.py``` by using ```ROOT/ROOT/settings.py``` as a template. 

### GnuPG

You need at least ```libgpgme(-dev) >= 1.8.0``` installed on your system.

If your gnupg is older than 2.1.12 you need to add the following option in your ```~/.gnupg/gpg-agent.conf```:

Import your signature key and your managing director key into this keyring as you defined in the setting variables ```GPG_HOST_USER``` and ```GPG_MANAGING_DIRECTOR```

```
allow-loopback-pinentry
```

Make sure the folder ```ROOT/<GPG_HOME_FOLDER>``` exists, where GPG_HOME_FOLDER is the name you defined in the ```ROOT/ROOT/settings_production.py```, since it is not created automatically. In this folder add a file ```gpg.conf``` stating the keyserver to use:

```
keyserver hkp://keys.gnupg.net
```

### Cron

In order for periodic tasks to function a crontab entry is needed like the following:
```
* * * * *   /path/to/installation/env/bin/python3 /path/to/installation/ROOT/manage.py scheduler
```
This will run every minute and initiate all periodic tasks.

## Run

Calling ```install_and_start_django_server.sh``` will create a virtual environment, install all necessary Python
dependencies, create a Django user, migrate all database migrations and start a webserver on 127.0.0.1:8000.

On subsequent starts only ```start_server.sh``` is needed to bring up the webserver.

## Install on Vagrant

Start Vagrant
```bash
cd AA
vagrant up
vagrant ssh
```

Within the VM
```bash
cd /vagrant
./install_and_start_django_server_vagrant.sh
```
You will be prompted for the DB user and password.


## Install with Docker
Requires: docker, docker-compose

Open docker-compose.yml in your text editor and change the values after ADMIN_*= to something else; they determine the admin login.

To start
```bash
cd AA
docker-compose up -d
```
Ommit -d to start in attached mode allowing you to see console output.

Data is persisted using a volume one the docker host at /var/lib/docker/persistent-volumes/pythonfoo/AA/.
On osx and windows the docker daemon can't run natively, therefore it runs in a minimalistic vm, the docker-machine. On those system you need to use
```bash
docker-machine ssh
```
from a docker enabled shell to access the volume (e.g. to clear its contents)

You can also use SSHFS to mount a folder from your host system into the docker-machine, on osx there is a tool called docker-machine-nfs available which can be installed via homebrew. The use of the virtualbox shared folder is possible in theory but has a major performance impact on disk operations.

To shut down
```bash
cd AA
docker-compose down
```

After starting the container for the first time, the admin user will be written to the database which is persisted on the volume. The Environment variables are no longer necessary after that point and can be commented out or be removed from the docker-compose.yml followed by a restart of the container to make them unavailable within the container.


Notes:
A non file based database should not be installed within the same container but a separate one defined as a second service in the docker-compose.yml and is reachable from all other containers started from that docker-compose.yml by using the service name as host name as is with all other services exposing Ports in their dockerfile.
To add an nginx reverse-proxy for example add a new container with an nginx, exposing a port mapped to the outside in the docker-compose.yml. Since the nginx will be the the service reachable form the outside, the port mapping for django could be removed from the docker-compose.yml after the nginx is configured to redirect requests to django:8000

## Testing Emails

For testing emails set up a local mailserver which accepts all incoming emails and displays them.

A simple built-in version in Python is the following server, which prints the received emails to the console:
```bash
$ python -m smtpd -n -c DebuggingServer localhost:1025
```

maildump offers a webinterface on port 1080, but is only available for Python 2.

```bash
$ maildump -f
```

Both servers listen on port 1025 for incoming emails.

## Credit

Thanks to the people from the Pythonfoo at Chaosdorf for building this software. Without you the office would still
struggle with outdated DOS database technology.
