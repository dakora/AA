Changelog
=========


1.9.4 (2017-12-02)
------------------

Fix
~~~
- Another crash when sending Erfa statistik. [Jan Girlich]


1.9.3 (2017-12-01)
------------------

Fix
~~~
- Crash in monthly Erfastatistik job. [Jan Girlich]


1.9.2 (2017-12-01)
------------------

Fix
~~~
- Send Erfastatistik every month. [Jan Girlich]


1.9.1 (2017-12-01)
------------------

Fix
~~~
- Crash when changing the balance of a member. [Jan Girlich]


1.9.0 (2017-12-01)
------------------

New
~~~
- Send an email explaining what's going on when moving a Doppelmitglied
  to a different Erfa. [Jan Girlich]

Fix
~~~
- Also find exited members. Necessary for example when they still have
  unpaid fees. [Jan Girlich]
- Do not send an exit email for Doppelmitglieder. [Jan Girlich]
- Use the admin interface for editing Doppelmitgliedschaft members when
  using /members/select_erfa/ [Jan Girlich]


1.8.0 (2017-11-29)
------------------

New
~~~
- Add a transaction message field to the member edit form which has to
  be filled in when changing the account balance. [Jan Girlich]
- Add functionality to easily move members out of an Erfa. [Jan Girlich]

  Entry point at /members/select_erfa/


1.7.1 (2017-11-21)
------------------

Fix
~~~
- Use correct gpg syntax for version 1.8.0 because that is the commonly
  available version. [Jan Girlich]


1.7.0 (2017-11-20)
------------------

New
~~~
- Run tests in parallel and ensure all tests are run. [Jan Girlich]
- Run tests in parallel and show Python warnings. [Jan Girlich]
- Prepare codebase for Django 2.0 by fixing all deprecation warnings.
  [Jan Girlich]
- Redo the Erfastatistik. New design, easier code, and fixes in the
  data. [Jan Girlich]
- Do not allow changing of membership type. This requires a new
  membership application. Honorary members can only be appointed by the
  general assembly. [Jan Girlich]
- Template for systemd service. [Jan Girlich]
- Reverting transactions frees the connected bank import, if any, to
  allow it to associate it again. [Jan Girlich]

Fix
~~~
- Remove outdated import statements. [Jan Girlich]
- Add missing dev requirement for test fixtures. [Jan Girlich]
- Install dev requirements for running tests. [Jan Girlich]
- Add database migration for reverting balance transaction logs
  connected to a bank import transaction. [Jan Girlich]
- Crash when reverting a balance transaction without bank transaction
  connected to it. [Jan Girlich]
- Crash in generating the Erfastatistik. [Jan Girlich]
- Avoid adding new membership fees past the membership end date. [Jan
  Girlich]
- Make account balance explanation in emails easier to understand. [Jan
  Girlich]

Other
~~~~~
- Revert "Fix: Add database migration for reverting balance transaction
  logs connected to a bank import transaction" [Jan Girlich]

  This reverts commit f86d2f591438037e4aa38d10f628ff8644cde2e1.


1.6.0 (2017-10-17)
------------------

New
~~~
- Handle member resignation throuhg the membership end field. [Jan
  Girlich]

  This removes the exit member button on the search results page
  and adds a cron job to remove members from the database daily.
  Resigned members will be deleted from the database instead of
  leaving an empty stub.
- Run the billing cycle automatically in the background daily. [Jan
  Girlich]
- Add a scheduler for periodic tasks. [Jan Girlich]
- Add a German GPG error email template. [Jan Girlich]
- Show the previous and next due date on BillingCycle transactions. [Jan
  Girlich]
- Add more strings to detect donations in bank imports. [Jan Girlich]

Changes
~~~~~~~
- Better explain GPG key problems in gpg error email. [Jan Girlich]
- Better describe how membership fees work in welcome and data set
  mails. [Jan Girlich]

Fix
~~~
- Repair search for members without email address. [Jan Girlich]
- Properly format error message when email template is missing required
  blocks. [Jan Girlich]
- Move pagination back underneath table in transaction log view and
  manage transaction view. [Jan Girlich]
- Revert billing when saving a member because this caused doubled
  billings and failed bank imports. [Jan Girlich]
- Remove TODO line from welcome email. [Jan Girlich]


1.5.4 (2017-09-01)
------------------

Fix
~~~
- Only try to change gpg key ID to fingerprint if a fingerprint is
  available. [Jan Girlich]


1.5.3 (2017-09-01)
------------------

Fix
~~~
- Remove real gpg keys from test keyring. [Jan Girlich]
- Use GPG_HOME from settings for gpg keyring. [Jan Girlich]
- Minor HTML mistake in transaction view. [Jan Girlich]


1.5.2 (2017-09-01)
------------------

New
~~~
- Add button for a transaction log per member to the member search
  results. [Jan Girlich]


1.5.1 (2017-08-30)
------------------

New
~~~
- Add links for sending all emails and Erfastatistik to UI. [Jan
  Girlich]
- Once a gpg key is used, store its fingerprint instead of its ID for
  future use. [Jan Girlich]

Changes
~~~~~~~
- Cleanup unused, old gpg code and fields. [Jan Girlich]

Fix
~~~
- Show number of emails in queue. [Jan Girlich]
- Making sure only one signing key is used for gpg emails. [Jan Girlich]
- Proper JSON output for several api functions. [Jan Girlich]
- Store gpg error message in member dataset. [Jan Girlich]
- Make gpg fingerprint field in admin large enough for full fingerprint.
  [Jan Girlich]


1.5.0 (2017-08-21)
------------------

New
~~~
- Use official gpgme Python bindings for encrypting and signing emails.
  [Jan Girlich]


1.4.0 (2017-08-10)
------------------

New
~~~
- Check if membership payment is due when modifying a member and adjust
  next payment date and balance accordingly. [Jan Girlich]
- Set balance to 0 and next due date to today for reactivated members.
  [Jan Girlich]
- Add validator and remove spaces for gpg key id field. [Jan Girlich]
- GPG-sign all outgoing emails. [Jan Girlich]
- Validation that Doppelmitgliedschafts-Erfas have no supporters. [Jan
  Girlich]
- More test data to import and work with and a mail configuration
  suitable for fake SMTPs like mailhub. [Jan Girlich]

Changes
~~~~~~~
- Make the gpg error messages more verbose. [Jan Girlich]
- Adjusting to one unittest file per class for better standards
  compatibility. [Jan Girlich]
- Updated welcome e-mail with additional information for new members.
  [Jan Girlich]
- Minor code improvements in member import. [Jan Girlich]

Fix
~~~
- Properly disable registration fee field in admin interface. [Jan
  Girlich]
- Remove empty () after Alien. [Jan Girlich]
- Catch empty gpg encrypted emails and handle them as an unknown error.
  [Jan Girlich]


1.3.0 (2017-07-18)
------------------

New
~~~
- Clearly state Verwendungszweck in email templates. [Jan Girlich]
- Sort Erfas in Erfastatistik alphabetically. [Jan Girlich]

Fix
~~~
- Enable entering a negative balance in admin interface. [Jan
  Girlich]
- Properly calculate the overdue membership fees for the Erfa
  statistics. [Jan Girlich]

Other
~~~~~
- Only paid until date and active status is checked before
  subtracting annual fee from balance. [Jan Girlich]
- Split members and supporters in Erfastatistik. [Jan Girlich]
- More detail in representing different member types in the
  Erfastatistik. [Jan Girlich]
- Resolve "Discussion: How should the billing cycle handle an
  insufficient balance when the next fee is due?" [Jan Girlich]


1.2.4 (2017-06-11)
------------------

Fix
~~~
- Crash on reverting a transaction. [Jan Girlich]
- Set last paid date only on bank import. [Jan Girlich]


1.2.3 (2017-05-27)
------------------

Fix
~~~
- Set last paid date when the balance is increased or set. [Jan Girlich]
- Force Erfa to Alien if none is set and disallow setting no Erfa. [Jan Girlich]


1.2.2 (2017-05-19)
------------------

Fix
~~~
- Show email addresses in welcome mail. [Jan Girlich]
- Do not ask new members who already paid to pay again
  [Jan Girlich]


1.2.1 (2017-05-11)
------------------

New
~~~
- Automatically determine the version number for the documentation from
  the latest git tag. [Jan Girlich]
- Use gitchangelog to directly use commit messages for CHANGELOG
  generation. [Jan Girlich]

  A short excerpt from the gitchangelog documentation about how to format the commit messages:

  Format

    ACTION: [AUDIENCE:] COMMIT_MSG [!TAG ...]

  Description

    ACTION is one of 'chg', 'fix', 'new'

        Is WHAT the change is about.

        'chg' is for refactor, small improvement, cosmetic changes...
        'fix' is for bug fixes
        'new' is for new features, big improvement

    AUDIENCE is optional and one of 'dev', 'usr', 'pkg', 'test', 'doc'

        Is WHO is concerned by the change.

        'dev'  is for developpers (API changes, refactors...)
        'usr'  is for final users (UI changes)
        'pkg'  is for packagers   (packaging changes)
        'test' is for testers     (test only related changes)
        'doc'  is for doc guys    (doc only changes)

    COMMIT_MSG is ... well ... the commit message itself.

    TAGs are additionnal adjective as 'refactor' 'minor' 'cosmetic'

        They are preceded with a '!' or a '@' (prefer the former, as the
        latter is wrongly interpreted in github.) Commonly used tags are:

        'refactor' is obviously for refactoring code only
        'minor' is for a very meaningless change (a typo, adding a comment)
        'cosmetic' is for cosmetic driven change (re-indentation, 80-col...)
        'wip' is for partial functionality but complete subfunctionality.

  Example:

    new: usr: support of bazaar implemented
    chg: re-indentend some lines !cosmetic
    new: dev: updated code to be compatible with last version of killer lib.
    fix: pkg: updated year of licence coverage.
    new: test: added a bunch of test around user usability of feature X.
    fix: typo in spelling my name in comment. !minor

    Please note that multi-line commit message are supported, and only the
    first line will be considered as the "summary" of the commit message. So
    tags, and other rules only applies to the summary.  The body of the commit
    message will be displayed in the changelog without reformatting.

Fix
~~~
- Crash when a candidate for a chaos numbers > max_int was found in a
  swift transfer comment. [Jan Girlich]
- Make migrations for import_app work. [Jan Girlich]
- Crash when a swift transaction message is just made up of punctuation.
  [Jan Girlich]


1.2.0 (2017-05-04)
------------------

New
~~~
- Import for Vereinstisch CSVs was adjusted to the new format

Fix
~~~
- No emails are sent to inactive members or members of a Doppelmitgliedschafts-Erfa


1.1.1 (2017-04-26)
------------------

Fix
~~~
- Add missing migration for new email type


1.1.0 (2017-04-06)
------------------

New
~~~
- Send a confirmation email when a member is leaving
- Comment field is now shown when entering a new member
- Default for Erfa is now 'Alien' and for country 'Germany'

Fix
~~~
- Search by email addresses did not work
- Empty emails after gpg encryption are now caught and an error notification sent
- A crash on gpg encryption errors was fixed


1.0.0 (2017-04-20)
------------------
- First usable version. The software is in a state that the most important
  workflows of the CCC office are implemented and that it can be used to
  replace the old DBaseII software.

