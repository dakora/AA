FROM ubuntu:zesty

RUN apt-get update && apt-get install -y python3 python3-pip python-virtualenv libgpg-error-dev

ADD requirements.txt /setup/requirements.txt

ADD ROOT /setup/ROOT

RUN pip3 update

RUN pip3 install -r /setup/requirements.txt

ADD ./docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

EXPOSE 8000
